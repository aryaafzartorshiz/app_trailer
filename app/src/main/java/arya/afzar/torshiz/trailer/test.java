package arya.afzar.torshiz.trailer;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class test extends AppCompatActivity {

    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        videoView=findViewById(R.id.video);
        final MediaController mediaController =new MediaController(this);
        videoView.setMediaController(mediaController);

        Uri uri=Uri.parse("https://as8.cdn.asset.aparat.com/aparat-video/996539e954a9125ccb1113ccac87f6f425370639-240p.mp4?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImI4ZmY4ZmU0ODlhNzc3NDI1MTIwNjZkYzJlNDk4NjVkIiwiZXhwIjoxNjA3MzYwOTA0LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.8ibtfg6h-s0LOM46cOIzYxFNs5Q1NdVSIrpscI-l4Hs");
        videoView.setVideoURI(uri);

        videoView.start();
    }
}
