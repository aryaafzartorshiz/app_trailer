package arya.afzar.torshiz.trailer.Models;

import com.google.gson.annotations.SerializedName;

public class TrailerList {

    @SerializedName("ID")
    private String ID;

    @SerializedName("namefilm")
    private String namefilm;

    @SerializedName("url")
    private String url;

    @SerializedName("imdb")
    private int imdb;

    @SerializedName("daste_int")
    private int daste_int;

    @SerializedName("daste_title")
    private String daste_title;

    @SerializedName("descrip")
    private String descrip;

    @SerializedName("IDfilm")
    private int IDfilm;


    @SerializedName("pic")
    private String pic;


    public TrailerList(String ID, String namefilm, String url, int imdb, int daste_int, String daste_title, String descrip, int IDfilm, String pic) {
        this.ID = ID;
        this.namefilm = namefilm;
        this.url = url;
        this.imdb = imdb;
        this.daste_int = daste_int;
        this.daste_title = daste_title;
        this.descrip = descrip;
        this.IDfilm = IDfilm;
        this.pic = pic;
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNamefilm() {
        return namefilm;
    }

    public void setNamefilm(String namefilm) {
        this.namefilm = namefilm;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getImdb() {
        return imdb;
    }

    public void setImdb(int imdb) {
        this.imdb = imdb;
    }

    public int getDaste_int() {
        return daste_int;
    }

    public void setDaste_int(int daste_int) {
        this.daste_int = daste_int;
    }

    public String getDaste_title() {
        return daste_title;
    }

    public void setDaste_title(String daste_title) {
        this.daste_title = daste_title;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public int getIDfilm() {
        return IDfilm;
    }

    public void setIDfilm(int IDfilm) {
        this.IDfilm = IDfilm;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
